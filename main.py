from fastapi import FastAPI
import uvicorn
app = FastAPI()

@app.post(
    "/generate_planning", 
    response_description="The planning that was generated."
)
async def generate_planning(request: any):
    print(request)
    return []
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)