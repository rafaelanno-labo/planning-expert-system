from datetime import datetime, timedelta
from itertools import groupby
import random
from typing import Dict, List, Union
from pydantic import BaseModel, Field
class TeacherPreference(BaseModel):
    teacherId: int
    matters: List[int]
class MatterPreference(BaseModel):
    matterId: int = Field(description="ID of the class")
    number_of_minutes: int = Field(description="Number of minute asked by the class")
class ClassPreference(BaseModel):
    classId: int = Field(description="Number of minute asked by the class")
    mattersPreferences: List[MatterPreference] = Field(description="What matter in which amount")
class RequestBodyModel(BaseModel):
    teachersPreferences: List[TeacherPreference] = Field(description="All teacher preferences", example=[
        {
            "teacherId": 1,
            "matters": [1]
        },
        {
            "teacherId": 2,
            "matters": [3]
        },
        {
            "teacherId": 3,
            "matters": [4, 5]
        }
    ])
    classesPreferences: List[ClassPreference] = Field(description="All class preferences", example=[
        {
            "classId": 1,
            "mattersPreferences": [{
                "matterId":1,
                # For 40 hours
                "number_of_minutes":2400
            },{
                "matterId":3,
                # For 40 hours
                "number_of_minutes":2400
            }]
        },
        {
            "classId": 2,
            "mattersPreferences": [{
                "matterId":4,
                # For 40 hours
                "number_of_minutes":2400
            }]
        }
    ]) 
