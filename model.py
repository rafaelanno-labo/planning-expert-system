from ortools.sat.python import cp_model

def main():
    # Créer le modèle.
    model = cp_model.CpModel()

    # Définir les données.
    matieres = ['Mathématiques', 'Physique', 'Chimie', 'Histoire', 'Géographie']
    profs = ['Prof A', 'Prof B', 'Prof C', 'Prof D', 'Prof E']
    classes = ['Classe 1', 'Classe 2', 'Classe 3']
    salles = ['Salle 1', 'Salle 2', 'Salle 3', 'Salle 4']
    jours = 90
    periodes_par_jour = 6
    total_periodes = jours * periodes_par_jour
    durees_periodes = [90]  # Durées possibles en minutes

    # Définir le nombre d'heures requis pour chaque matière par classe
    heures_requises = {
        'Classe 1': {'Mathématiques': 4 * 20 * 60, 'Physique': 3 * 20 * 60, 'Chimie': 2 * 20 * 60, 'Histoire': 3 * 20 * 60, 'Géographie': 2 * 20 * 60},
        'Classe 2': {'Mathématiques': 3 * 20 * 60, 'Physique': 4 * 20 * 60, 'Chimie': 3 * 20 * 60, 'Histoire': 2 * 20 * 60, 'Géographie': 2 * 20 * 60},
        'Classe 3': {'Mathématiques': 3 * 20 * 60, 'Physique': 2 * 20 * 60, 'Chimie': 4 * 20 * 60, 'Histoire': 3 * 20 * 60, 'Géographie': 3 * 20 * 60},
    }

    disponibilites = {
        'Mathématiques': {'Prof A': [1] * total_periodes, 'Prof B': [0] * total_periodes, 'Prof C': [1] * total_periodes, 'Prof D': [0] * total_periodes, 'Prof E': [0] * total_periodes},
        'Physique': {'Prof A': [0] * total_periodes, 'Prof B': [1] * total_periodes, 'Prof C': [0] * total_periodes, 'Prof D': [1] * total_periodes, 'Prof E': [0] * total_periodes},
        'Chimie': {'Prof A': [0] * total_periodes, 'Prof B': [0] * total_periodes, 'Prof C': [0] * total_periodes, 'Prof D': [1] * total_periodes, 'Prof E': [1] * total_periodes},
        'Histoire': {'Prof A': [1] * total_periodes, 'Prof B': [0] * total_periodes, 'Prof C': [0] * total_periodes, 'Prof D': [0] * total_periodes, 'Prof E': [1] * total_periodes},
        'Géographie': {'Prof A': [0] * total_periodes, 'Prof B': [1] * total_periodes, 'Prof C': [1] * total_periodes, 'Prof D': [0] * total_periodes, 'Prof E': [0] * total_periodes}
    }

    # Définir une tolérance (en minutes)
    tolerance = 60

    # Variables de décision.
    affectation = {}
    for classe in classes:
        for matiere in matieres:
            for periode in range(total_periodes):
                for prof in profs:
                    for salle in salles:
                        for duree in durees_periodes:
                            affectation[(classe, matiere, periode, prof, salle, duree)] = model.NewBoolVar(f'{classe}_{matiere}_{periode}_{prof}_{salle}_{duree}')

    # Variables de surplus et de déficit pour chaque matière par classe
    surplus = {}
    deficit = {}
    for classe in classes:
        for matiere in matieres:
            surplus[(classe, matiere)] = model.NewIntVar(0, tolerance, f'surplus_{classe}_{matiere}')
            deficit[(classe, matiere)] = model.NewIntVar(0, tolerance, f'deficit_{classe}_{matiere}')

    # Contraintes.
    # Une seule matière par période par classe
    for classe in classes:
        for periode in range(total_periodes):
            model.Add(sum(affectation[(classe, matiere, periode, prof, salle, duree)] for matiere in matieres for prof in profs for salle in salles for duree in durees_periodes) <= 1)

    # Une seule matière par période par professeur
    for periode in range(total_periodes):
        for prof in profs:
            model.Add(sum(affectation[(classe, matiere, periode, prof, salle, duree)] for classe in classes for matiere in matieres for salle in salles for duree in durees_periodes) <= 1)

    # Chaque matière doit être enseignée un nombre spécifique d'heures par semaine pour chaque classe avec tolérance
    for classe in classes:
        for matiere in matieres:
            heures_enseignées = sum(affectation[(classe, matiere, periode, prof, salle, duree)] * duree for periode in range(total_periodes) for prof in profs for salle in salles for duree in durees_periodes)
            model.Add(heures_enseignées == heures_requises[classe][matiere] + surplus[(classe, matiere)] - deficit[(classe, matiere)])

    # Respecter les disponibilités des professeurs
    for classe in classes:
        for matiere in matieres:
            for periode in range(total_periodes):
                for prof in profs:
                    for salle in salles:
                        for duree in durees_periodes:
                            if disponibilites[matiere][prof][periode] == 0:
                                model.Add(affectation[(classe, matiere, periode, prof, salle, duree)] == 0)

    # Empêcher la répétition de la même matière dans des périodes consécutives pour chaque classe
    for classe in classes:
        for jour in range(jours):
            for periode in range(periodes_par_jour - 1):
                for matiere in matieres:
                    current_period = jour * periodes_par_jour + periode
                    next_period = current_period + 1
                    for prof in profs:
                        for salle in salles:
                            for duree in durees_periodes:
                                model.Add(affectation[(classe, matiere, current_period, prof, salle, duree)] + affectation[(classe, matiere, next_period, prof, salle, duree)] <= 1)

    # Une seule classe par salle à une période donnée
    for periode in range(total_periodes):
        for salle in salles:
            model.Add(sum(affectation[(classe, matiere, periode, prof, salle, duree)] for classe in classes for matiere in matieres for prof in profs for duree in durees_periodes) <= 1)

    # Define which classes have sessions only on even weeks, only on odd weeks, or on both
    week_constraints = {
        'Classe 1': 'both',  # Can have sessions in both even and odd weeks
        'Classe 2': 'even',  # Can have sessions only in even weeks
        'Classe 3': 'odd'    # Can have sessions only in odd weeks
    }

    # Constraint for even and odd week scheduling
    for classe in classes:
        for periode in range(total_periodes):
            jour = periode // periodes_par_jour
            semaine = jour // 5  # Each week has 5 days
            is_even_week = (semaine % 2 == 0)
            for matiere in matieres:
                for prof in profs:
                    for salle in salles:
                        for duree in durees_periodes:
                            if week_constraints[classe] == 'even' and not is_even_week:
                                model.Add(affectation[(classe, matiere, periode, prof, salle, duree)] == 0)
                            elif week_constraints[classe] == 'odd' and is_even_week:
                                model.Add(affectation[(classe, matiere, periode, prof, salle, duree)] == 0)

    # Résoudre le modèle.
    solver = cp_model.CpSolver()
    status = solver.Solve(model)

    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        print('Solution:')
        for classe in classes:
            print(f'\nEmploi du temps pour {classe}:')
            for periode in range(total_periodes):
                for matiere in matieres:
                    for prof in profs:
                        for salle in salles:
                            for duree in durees_periodes:
                                if solver.Value(affectation[(classe, matiere, periode, prof, salle, duree)]):
                                    jour = periode // periodes_par_jour
                                    slot = periode % periodes_par_jour
                                    print(f'Jour {jour+1}, Période {slot+1}: {matiere} par {prof} en {salle} pour {duree} minutes')
            for matiere in matieres:
                surplus_value = solver.Value(surplus[(classe, matiere)])
                deficit_value = solver.Value(deficit[(classe, matiere)])
                print(f'{matiere}: Surplus = {surplus_value} minutes, Déficit = {deficit_value} minutes')
    else:
        print('Pas de solution trouvée.')

if __name__ == '__main__':
    main()
